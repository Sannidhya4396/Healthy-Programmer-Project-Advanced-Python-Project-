# I have used only one recordinfg for all of the functions of water, eyes and exercise. You can different sounds for different options and add more functions if you want
# make sure you have all the imported modules installed
from pygame import mixer
from datetime import datetime
from time import time


def musiconloop(file, stopper):
    mixer.init()
    mixer.music.load(file)
    mixer.music.play()
    while True:
        a = input("\n")
        if a == stopper:
            mixer.music.stop()
            break


def log_now(msg):
    with open("mylogs.txt", "a") as f:
        f.write(f"{msg} {datetime.now()}\n")


if __name__ == "__main__":
    # musiconloop("house_lo.mp3", "s")
    init_water = time()
    init_eyes = time()
    init_exercise = time()
    watersecs = 5
    eyessecs = 10
    exersicesecs = 15

    while True:
        if time() - init_water > watersecs:
            print("It's time to drink water dude! Enter 'd' to stop")
            musiconloop("house_lo.mp3", "d")
            init_water = time()
            log_now("Drank Water at:")
            q = input("press q to quit or c to continue\n")
            if q == "q":
                exit()
            elif q == "c":
                continue
            else:
                print("Invalid input, but still alowing to continue")
        if time() - init_eyes > eyessecs:
            print("It's time for eye rest dude! Enter 'e' to stop")
            musiconloop("house_lo.mp3", "e")
            init_eyes = time()
            log_now("Gave eyes rest at:")
            q = input("press q to quit or c to continue\n")
            if q == "q":
                exit()
            elif q == "c":
                continue
            else:
                print("Invalid input, but still allowing to continue")

        if time() - init_exercise > exersicesecs:
            print("It's time for some exercise dude! Enter 'x' to stop")
            musiconloop("house_lo.mp3", "x")
            init_exercise = time()
            log_now("Did physical exercise at:")
            q = input("press q to quit or c to continue\n")
            if q == "q":
                exit()
            elif q == "c":
                continue
            else:
                print("Invalid input, but still alowing to continue")
